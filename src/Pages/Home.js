import React from 'react'
import NewCarousel  from "../Component/NewCarousel";
import AllMovies from "../Component/AllMovies"
import MovieSearch from "../Component/MovieSearch"

const Home = () => {

  return (
    <div>
      <NewCarousel/>
      <MovieSearch/>
      <AllMovies/>
    </div>
  );
}

export default Home;