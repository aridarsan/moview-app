import React from 'react'
import Character from '../Component/Character'
import ConclusionReview from '../Component/ConclusionReview'
// import Overview from '../Component/Overview'
import ReviewDisplay from '../Component/ReviewDisplay'
import WriteReview from '../Component/WriteReview'

const DetailPageMovie = () => {
  return (
    <div>
      <ConclusionReview/>
      <Character/>
      <WriteReview/>
      <ReviewDisplay/>
      {/* <Overview/> */}
    </div>
  )
}

export default DetailPageMovie;