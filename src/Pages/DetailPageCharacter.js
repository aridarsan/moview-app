import React from 'react';
import Character from '../Component/Character';
import ConclusionReview from '../Component/ConclusionReview';

const DetailPageCharacter = () => {
  return (
    <div>
      <ConclusionReview/>
      <Character/>
    </div>
  )
}

export default DetailPageCharacter;