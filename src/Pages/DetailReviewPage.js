import React from 'react'
// import ConclusionReview from '../Component/ConclusionReview'
import ReviewDisplay from '../Component/ReviewDisplay'

const DetailReviewPage = () => {
  return (
    <div>
      {/* <ConclusionReview/> */}
      <ReviewDisplay/>
    </div>
  )
}

export default DetailReviewPage;