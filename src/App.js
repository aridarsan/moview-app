import React from 'react';
import {Switch, Route} from 'react-router-dom';
import NavBar from './Component/NavBar';
import Footer from './Component/Footer';
import Home from './Pages/Home';
import Register from './Component/Register';
import Login from './Component/Login'
import DetailPageMovie from './Pages/DetailPageMovie';
import DetailPageCharacter from './Pages/DetailPageCharacter';
import DetailReviewPage from './Pages/DetailReviewPage';
import ProfilPage from './Pages/ProfilPage';
import SearchPage from './Pages/SearchPage';
import Logout from './Component/Logout';

const App = () => {
  return (
    <>
      <NavBar/>
        <Switch>

          <Route exact path="/register">
            <Register/>
          </Route>

          <Route exact path="/login">
            <Login/>
          </Route>

          <Route exact path="/detail/:movieId">
            <DetailPageMovie/>
          </Route>

          {/* <Route exact path="/trailer/:movieId">
            <TrailerWatchPage/>
          </Route> */}

          {/* <Route exact path="/trailer/:movieId">
            <TrailerWatch/>
          </Route> */}

          <Route exact path="/character/:MovieId">
            <DetailPageCharacter/>
          </Route>

          <Route exact path="/review/:movieId">
            <DetailReviewPage/>
          </Route>

          <Route exact path="/profil">
            <ProfilPage/>
          </Route>

          <Route exact path="/search">
            <SearchPage/>
          </Route>

          <Route exact path="/logout">
            <Logout/>
          </Route>

          <Route exact path="/">
            <Home/>
          </Route>

        </Switch>
      <Footer/>
    </>
  )
}

export default App;
