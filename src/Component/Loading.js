import React from 'react'
import { Container, Spinner } from 'reactstrap'

export default function Loading() {
  return (
    <Container className="d-flex align-items-center justify-content-center">
      <Spinner 
        style={{ width: '7rem', height: '7rem' }}
        color="warning"/>
    </Container>
  )
}