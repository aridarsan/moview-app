import ReactStars from "react-rating-stars-component";
import { 
  Container, 
  Form, 
  FormGroup, 
  Label,
  Input,
  Button, 
} from 'reactstrap';
import React, { useState, useEffect } from "react";
import axios from "axios";
import '../Styles/WriteReview.css'

const WriteReview = () => {
  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");
  const [reviewData, setReviewData] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios
      .get("https://5fa4bcd2732de900162e85ef.mockapi.io/api/reviews")
      .then((res) => {
        localStorage.setItem("dataReview", res.data);
        setReviewData(res.data);
      });
  };

  const submitReview = (e) => {
    e.preventDefault();

    const data = {
      rating: rating,
      review: review
    };
    axios
      .post("https://5fa4bcd2732de900162e85ef.mockapi.io/api/reviews", data)
      .then((res) => {
        console.log(res);
        getData();
      })
      .catch((err) => console.log(err));
  };

  return (
    <Container className="write">
        <p>{reviewData}</p>
        <Form className="col-sm-12 col-md-12" onSubmit={submitReview}>
        <h3 style={{ color:"#E09F3E"}}> Please Leave your Review</h3>
          <FormGroup>
            <Label for="select" style={{ color:"#E09F3E"}}>Select Rating</Label>
            <ReactStars count={5} size={50} onChange={(e) => setRating(e.target.value)} />
          </FormGroup>

          <FormGroup>
            <Label for="text" style={{ color:"#E09F3E"}}>Write your review</Label>
            <Input type="textarea" name="text" id="text" onChange={(e) => setReview(e.target.value)} />
          </FormGroup>
          <Button 
          style={{backgroundColor: "#E09F3E", color: "#222222"}}
          type="submit"><b>Submit</b></Button>
      </Form>
    </Container>
  );
};

export default WriteReview;