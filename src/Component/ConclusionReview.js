import React from "react";
import { Button, Container, Jumbotron } from "reactstrap";
import { useParams, useHistory} from "react-router-dom";
import { useEffect } from "react";
import axios from "axios";
import { useState } from "react";
import Loading from './Loading';
import Rating from '@material-ui/lab/Rating'
import TrailerWatch from './TrailerWatch'


const ConclusionReview = () => {
  const imgUrl = "https://image.tmdb.org/t/p/original";

  const params = useParams();
  const history = useHistory();

  const [movieDetails, setMovieDetails] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {

    document.body.style.backgroundColor = "#222222"
  
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${params.movieId}?api_key=8508a0bd1efc493c4bfa095b6a37f250&language=en-US`
      )
      .then((res) => {
        setMovieDetails(res.data);
        setLoading(true)        
      });
  },[params.movieId]);

  return (
    <Container>
      {movieDetails && loading ? (
        <div>
          <Jumbotron
            style={{
              backgroundImage: `url(${imgUrl}/${movieDetails.backdrop_path})`,
              backgroundSize: "cover",
              color: "white",
              minHeight: "350px",
              marginTop: "10px"
            }}
          >
            <h1 className="title-details"><b>{movieDetails.title}</b></h1>
            <Rating name="half-rating-read" defaultValue={movieDetails.vote_average} precision={0.1} max={10} readOnly /><p><b>{movieDetails.vote_average} / 10</b></p>
            <p>{movieDetails.tagline}</p>

            <div>
              <TrailerWatch  onClick={() => {
                  history.push(`${params.movieId}`);
                }} />
              {/* <Button className="mr-3" style={{backgroundColor: "#E09F3E", color: "#222222"}}>
                <strong></strong>
              </Button> */}
              <Button style={{backgroundColor: "#E09F3E", color: "#222222", outlineColor: '#E09F3E'}}>
                Add to Watchlist
              </Button>
            </div>
          </Jumbotron>
          {/* <div>
            <div>
              <Button className="mr-2 mb-2 rounded-pill" color="primary">
                Info
              </Button>
              <Button className="mr-2 mb-2 rounded-pill" color="light">
                Character
              </Button>
              <Button className="mr-2 mb-2 rounded-pill" color="light">
                Review
              </Button>
            </div>
            <div style={{color: "white"}}>
              <h2><strong>Synopsys</strong></h2>
              <p>{movieDetails.overview}</p>
              <Link to="/overview"><Button color="danger"><strong>Back</strong></Button></Link>
            </div>
          </div> */}
        </div>
      ) : (
        <Loading/>
      )}
    </Container>
  );
};

export default ConclusionReview;