/* eslint-disable jsx-a11y/iframe-has-title */
import React, { useState, useEffect } from "react";
import { Button, Container, Spinner, Col, Card, CardTitle, Row, Modal, ModalHeader, ModalBody} from 'reactstrap';
import { useParams } from "react-router-dom";
import axios from "axios";
import ReactPlayer from "react-player";

const TrailerWatch = () => {

  const apiUrl = "https://api.themoviedb.org/3/movie/";

  const params = useParams();

  const [results, setResults] = useState(null);

  
  const [modal, setModal] = useState(false);
  
  const toggle = () => setModal(!modal);
  

  useEffect(() => {
    axios
      .get(
        `${apiUrl}${params.movieId}/videos?api_key=8508a0bd1efc493c4bfa095b6a37f250&language=en-US&page=1`
      )
      .then((res) => {
        console.log(res.data.results)
        setResults(res.data.results);
      });
  }, [params.movieId]);

  return (
    <>
    <Button
      style={{backgroundColor: "#E09F3E", color: "#222222", outline:"none"}}
      onClick={toggle}
      className="mr-3"
      >Watch Trailer</Button>
      <Modal isOpen={modal} toggle={toggle} className="">
      <ModalHeader toggle={toggle}>Trailer available for this movie:</ModalHeader>
        <ModalBody>
        <Container>
      { results ? (
        <div>
          {results.map((result) => ( 
            <Row>
              <Col key={result.id}>
                <Card className="text-center">
                  <CardTitle><b>{result.name}</b></CardTitle>
                  <ReactPlayer url={`https://www.youtube.com/watch?v=${result.key}`} width="100%" height="400px" controls/>
                </Card>
              </Col>
            </Row>
            ))
          }
        </div>
      ) : ( <Spinner color="primary"/>)
      }
    </Container>
    </ModalBody>
    </Modal>
    </>
  )
}

export default TrailerWatch;