import React from "react";
import { Container, Row, Card, CardBody} from "reactstrap";



  const Carousel = (props) => {
  return (
    <Container className="carouselCard">
      <Row>
        <Card>
          <CardBody>
            <img
              width="100%"
              src="https://m.media-amazon.com/images/M/MV5BMGVmMWNiMDktYjQ0Mi00MWIxLTk0N2UtN2ZlYTdkN2IzNDNlXkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_SX300.jpg" alt="carousel"
            />
          </CardBody>
        </Card>        
      </Row>
    </Container>
  );
};

export default Carousel;
