import React, { Component } from "react";
import "antd/dist/antd.css";
import {  Modal, Button } from "antd";
import { Container } from "reactstrap";
import Avatar from "antd/lib/avatar/avatar";
import "@fortawesome/fontawesome-free";

class ProfilPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  render() {
    return (
      <Container className="vh-100">
        <div className="avatar">
        <h3>Hello</h3>
        <Avatar size={200}  />
        </div>
      <div className="AccountSetting">
        <Button type="primary" onClick={this.showModal}>
          Open Modal
        </Button>
        <Modal
          title="Basic Modal"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>Some contents...</p>
          <p>Some contents...</p>
          <p>Some contents...</p>
        </Modal>
      </div>
      </Container>
    );
  }
}
export default ProfilPage;
