import React, { useState } from 'react';
import { useHistory, Link } from 'react-router-dom';
import axios from 'axios';
import Cookies from "js-cookie";
import { 
  Button, 
  Modal, 
  ModalHeader, 
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input, 
} from 'reactstrap';

const Login = (props) => {
  
  const urlLogin = "https://5fad41ff2ec98b00160481c3.mockapi.io/movie/register"
  
  const history = useHistory();
  
  const {
    className
  } = props;

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    
    const data = {
      email: email,
      password: password,
    };

    axios
    .post(urlLogin, data)
    .then((res) => {
      const { username, role, token } = res.data;
      Cookies.set("username", username);
      Cookies.set("role", role);
      Cookies.set("token", token);
      history.push("/");
    });
  };

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
      <div>
        <Button className="mb-2 mr-2 mb-sm-0" color="info" onClick={toggle}>Sign In</Button>
        <Modal isOpen={modal} toggle={toggle} className={className}>
          <ModalHeader toggle={toggle}><strong>Please login to continue.</strong></ModalHeader>
            <ModalBody>
              <Form onSubmit={handleSubmit}>

                <FormGroup>
                  <Label for="email">Email</Label>
                    <Input type="email" name="email" id="email" placeholder="Input your email address." onChange={(e) => setEmail(e.target.value)}/>
                </FormGroup>

                <FormGroup>
                  <Label for="password">Password</Label>
                    <Input type="password" name="password" id="password" placeholder="Input your password." onChange={(e) => setPassword(e.target.value)}/> 
                </FormGroup>
  
                <FormGroup>
                  <Button block type="submit" style={{backgroundColor: "#E09F3E", color: "#222222"}}>Sign In</Button>
                </FormGroup>
                <br/>
                <p className="text-center">Don't have an account? You can sign up <Link to="/register">here!</Link></p>
              </Form>
            </ModalBody>
        </Modal>
      </div>
    ) 
};

export default Login;