/* eslint-disable no-undef */
import React, { useState, useEffect } from "react";
import axios from "axios";
import { Spinner, Container, Row, Card, Button, CardBody } from 'reactstrap';
import '../Styles/ReviewDisplay.css'

const ReviewDisplay = () => {

  const [reviewData, setReviewData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    getData();
    setIsLoading(false);
  }, []);

  const url = "https://5fad41ff2ec98b00160481c3.mockapi.io/movie/movies";

  const getData = () => {
    axios
      .get(url)
      .then((res) => {
        setReviewData(res.data)
      });
  };


  const handleDelete = (e, id) => {
    e.preventDefault();
    setIsLoading(true);

    const url = `https://5fad41ff2ec98b00160481c3.mockapi.io/movie/movies/${id}`;
    axios.delete(url)
    .then((res) => {
      getData();
      setIsLoading(false);
    });
  };

  return (
    <>
      <Container>
        {/* <Row>
            <div>
              <Link to="/detail">
                <Button className="mr-2 mb-2 rounded-pill" color="light">
                  Overview
                </Button>
              </Link>

              <Link to="/character">
                <Button className="mr-2 mb-2 rounded-pill" color="light">
                  Character
                </Button>
              </Link>

              <Link to="/review">
              <Button className="mr-2 mb-2 rounded-pill" color="primary"
                >
                Review
              </Button>
              </Link>
            </div>
        </Row> */}

        {isLoading &&  <Spinner color="primary" className="align-items-center justify-content-center" />}
        {!isLoading &&
          reviewData.map((data) => (
            <div>
              <Row>
                <img 
                src={data.avatar} 
                className="col-1" 
                style={{
                  width: "auto",
                  height: "4rem",
                  borderRadius: "20%",
                  marginTop: "20px"
                }} alt="avatar"
                />
                <Card 
                className="col-sm-11 mb-2" 
                style={{
                  backgroundColor:"#E09F3E",
                  border:"none",
                }}
                >
                  <CardBody key={data.id}>
                    <h3>{data.name}</h3>
                    <h4>Rating : {data.rating}</h4>
                    <p>{data.review}</p>
                    
                    <Button color="danger" onClick={(e) => handleDelete(e, data.id)}>Hapus</Button>
                  </CardBody>
                </Card>
              </Row>
            </div>
          ))}
      </Container>
    </>
  );
}

export default ReviewDisplay;