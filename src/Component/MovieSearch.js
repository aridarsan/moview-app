import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardText,
  CardTitle,
  Container,
  Form,
  FormGroup,
  Input,
  Row,
  Col,
} from "reactstrap";
import {withRouter} from "react-router-dom";

const urlAPI = "https://api.themoviedb.org/3/search/movie?api_key=d234beabba9f9d20d3b84e4beb89ce25&language=en-US&adult=false";
const imgUrl = "https://image.tmdb.org/t/p/w500";



class MovieSearch extends Component {
  constructor() {
    super();

    this.state = {
      movies: [],
      title: "",
      year: "",
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state.title, this.state.year);

    fetch(`${urlAPI}&query==${this.state.title}&year=${this.state.year}`)
      .then((res) => res.json())
      .then((json) => {
        this.setState({ movies: json.results });
      });
  };

  handleChangeTitle = (e) => {
    this.setState({
      title: e.target.value,
    });
  };

  handleChangeYear = (e) => {
    this.setState({
      year: e.target.value,
    });
  };

  render() {
    return (
      <div>
        <Container className="mb-2 mt-3 align-items-center">
          <Container>
            <Form inline onSubmit={this.handleSubmit}>
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Input
                  onChange={this.handleChangeTitle}
                  type="text"
                  name="title"
                  value={this.state.title}
                  placeholder="Title"
                />
              </FormGroup>

              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Input
                  onChange={this.handleChangeYear}
                  type="text"
                  name="year"
                  value={this.state.year}
                  placeholder="Year"
                />
              </FormGroup>

              <FormGroup className="mb-2 mr-sm-2 mb-sm-0 ml-2">
                  <Button color="warning"><strong>Search</strong></Button>
              </FormGroup>

            </Form>
          </Container>

          <Container className="mt-3 align-items-center">
            <Row>
          {this.state.movies.length !== 0
            ? this.state.movies.map((movie) => (
              <Col md={3} key={movie.id}>
                <Card style={{marginBottom: "15px", backgroundColor: "#E09F3E"}}>
                  <CardImg src={`${imgUrl}${movie.poster_path}`}></CardImg>
                  <CardBody className="d-flex flex-column align-items-center" style={{minHeight: "200px"}}>
                    <CardTitle style={{minHeight: "80px", marginTop:"15px"}}><strong>{movie.title}</strong></CardTitle>
                    <CardText>{movie.release_date}</CardText>
                    <Button 
                    block 
                    color="warning" 
                    className="mt-auto"
                    style={{backgroundColor:"#335C67", color:"whitesmoke"}}
                    onClick={() => this.props.history.push(`/detail/${movie.id}`)}
                    ><b>See Details</b></Button>
                  </CardBody>
                </Card>
              </Col>
              ))
              : ""}
            </Row>
            </Container>
        </Container>
    </div>
    );
  }
}

export default withRouter(MovieSearch);