import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "antd/dist/antd.css";
import { Button } from "antd";
import "./styles/PageProfil.css"
import {
  Form,
  FormGroup,
  Label,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
} from "reactstrap";
import "@fortawesome/fontawesome-free";


const AdminPage = (props) => {
  const [movieTitle, setMovieTitle] = useState("");
  const [rating, setRating] = useState("");
  const [production, setProduction] = useState("");
  const [director, setDirector] = useState("");
  const [cast, setCast] = useState("");

  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();

    const url = "https://5fa4bcd2732de900162e85ef.mockapi.io/api/register";
    const bodyData = {
        movieTitle: movieTitle,
        rating: rating,
        production: production,
        director: director,
    };

    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(bodyData),
    })
      .then((res) => res.json())
      .then((res) => console.log(res))
      .then(() => history.push("Categories"));
  };

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <ModalHeader className="modalHeader">
        <strong>Edit Profile</strong>
      </ModalHeader>
      <ModalBody className="modalBody">
        <Form onSubmit={handleSubmit}>
        <FormGroup>
            <Label for="movieTitle">Movie Title</Label>
            <Input
              type="text"
              name="movieTitle"
              id="movieTitle"
              placeholder="Input Movie Title."
              onChange={(e) => setMovieTitle(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label for="production">Production House</Label>
            <Input
              type="text"
              name="production"
              id="production"
              placeholder="Input Production house for this movie."
              onChange={(e) => setProduction(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label for="director">Director name</Label>
            <Input
              type="text"
              name="director"
              id="director"
              placeholder="Input Director name for this movie."
              onChange={(e) => setDirector(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label for="cast">Main Cast</Label>
            <Input
              type="text"
              name="cast"
              id="cast"
              placeholder="Input Main Cast for this movie."
              onChange={(e) => setCast(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label for="rating">Rating</Label>
            <Input
              type="text"
              name="rating"
              id="rating"
              placeholder="Input rating for this movie."
              onChange={(e) => setRating(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Button
              block
              type="submit"
              style={{ backgroundColor: "#E09F3E", color: "#222222" }}
            >
              Submit Your change
            </Button>
          </FormGroup>
        </Form>
      </ModalBody>
    </div>
  );
};

export default AdminPage;
