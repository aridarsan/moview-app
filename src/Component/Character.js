import {useEffect, useState} from 'react'
import { Spinner, Container, Row, Button, Card, CardBody, CardImg, CardText } from 'reactstrap'
import axios from "axios"
import {Link, useHistory, useParams} from 'react-router-dom'
import '../Styles/Character.css'

const Character = () => {

  const url = "https://5fad41ff2ec98b00160481c3.mockapi.io/movie/movies";

  const history = useHistory();
  const params = useParams();

  const [avatar, setAvatar] = useState([])

  useEffect(() => {
    axios
      .get(url)
      .then((res) => {
        console.log(res)
        setAvatar(res.data);
      });
  }, []);


  return (
    <Container className="text-center">
      <Row>
        <div>
          <Link to="/detail">
            <Button className="mr-2 mb-2 rounded-pill" color="primary">
              Overview
            </Button>
          </Link>

          <Link to="/character">
            <Button 
            className="mr-2 mb-2 rounded-pill" 
            color="light"                           
            onClick={() => {
            history.push(`/character/${params.movieId}`);
            }}>
              Character
            </Button>
          </Link>

          <Link to="/review/:movieId">
          <Button 
          className="mr-2 mb-2 rounded-pill" 
          color="light"
            >
            Review
          </Button>
          </Link>
        </div>
      </Row>
      <Row>
        { avatar ? (
          avatar.map((data) => (
            <Card
              className="col-sm-2 mb-2"
              style={{
                backgroundColor: "#E09F3E",
                color: "#222222",
                height: "220px",
                margin: "15px",
              }}>
                <CardBody key={data.id}>
                  <CardImg src={data.avatar} alt="avatar" className="avatar"/>
                  <CardText className="cardtext"><strong>{data.name}</strong></CardText>
                </CardBody>
              </Card>
          ))) : (<Spinner color="primary" className="align-items-center justify-content-center" />)
        }
      </Row>
      <hr style={{borderTop: "solid 5px #E09F3E"}}/>
    </Container>
  )
}

export default Character;