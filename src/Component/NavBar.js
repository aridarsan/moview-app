import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import Login from './Login'
import icon from '../icon/icon.png'
import { checkLogin } from "./Helper";
import Register from './Register';
import { 
  Container,
  Collapse,
  Row,
  Navbar,
  NavItem,
  NavbarBrand,
  NavbarToggler,
  Nav,
} from 'reactstrap'


const NavBar = () => {

  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return (
    <Navbar style={{backgroundColor: '#E09F3E'}} expand="md">
      <Container>
        <NavbarBrand>
            <img src={icon} alt="icon app" width="40px"/>
        </NavbarBrand>

        <NavbarToggler onClick={toggleNavbar} className="mr-2" />
        <Collapse isOpen={!collapsed} navbar>
          <Row color="white">
            <Nav className="mt-2 mb-2 mr-2 ml-2 ">

              <NavItem>
                <NavLink to="/" activeClassName="active" className="nav-link">
                  Home
                </NavLink>
              </NavItem>

              {!checkLogin() && (
                <>
              <NavItem>
                <Register/>
              </NavItem>

              <NavItem>
                <Login/>
              </NavItem>
              </>
              )}

              {checkLogin() && (
                <>
              {/* <NavItem>
                <ProfilPage/>
              </NavItem> */}

              <NavItem>
                <NavLink to="/logout" activeClassName="active" className="nav-link" color="white">
                  Logout   
                </NavLink>
              </NavItem>
                </>
              )}

            </Nav>
          </Row>
        </Collapse>
      </Container>
    </Navbar>
  )
}

export default NavBar;