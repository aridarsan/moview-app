import React, { Component } from "react";
import "@fortawesome/fontawesome-free";
import icon from "./../icon/icon.png";
import { Container, Col, Row } from 'reactstrap';
import '../Styles/Footer.css'

export default class Footer extends Component {
  render() {
    return (
      <Container fluid className="footer">
        <Row className="inside">
          <Col xs="6" sm="4" className="col text-center">
            <img className="logo my-auto" src={icon} alt="Logo"></img>
            <br/>
          </Col>
          <Col xs="6" sm="4" className="col">
            <h4>Designed and Created by:</h4>
            <hr className="line"/>
              <h5><a href="https://gitlab.com/aridarsan">Ari Darsan</a></h5>
              <h5><a href="https://gitlab.com/arszoran1992">Abdul Basith</a></h5>
              <h5><a href="https://gitlab.com/mdalvin">Mochamad Dalvin</a></h5>
          </Col>
          <Col xs="6" sm="4" className="col">
            <h4>Contact Us</h4>
            <hr className="line"/>
            <a href="https://www.facebook.com/">
              <i class="fab fa-facebook-square"></i>
            </a>
            <a href="https://twitter.com/home">
              <i class="fab fa-twitter-square"></i>
            </a>
            <a href="https://www.instagram.com/">
              <i class="fab fa-instagram"></i>
            </a>
          </Col>
        </Row>
      </Container>
    );
  }
}
