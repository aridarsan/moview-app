import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Loading from './Loading';
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardText,
  CardTitle,
  Col,
  Container,
  Row,
} from "reactstrap";
import axios from "axios";

function AllMovies() {

  const imgUrl = "https://image.tmdb.org/t/p/w500";
  const apiUrl = "https://api.themoviedb.org/3/";

  const history = useHistory();

  const [movies, setMovies] = useState([]);
  const [genres, setGenres] = useState([]);
  const [loading, setLoading] = useState(false);

  const [currentGenre, setCurrentGenre] = useState(null);

  useEffect(() => {

    document.body.style.backgroundColor = "#222222";

    axios
      .get(
        `${apiUrl}movie/popular?api_key=8508a0bd1efc493c4bfa095b6a37f250&language=en-US&page=1`
      )
      .then((res) => {
        setMovies(res.data.results);
        setLoading(true);
      });

    axios
      .get(
        `${apiUrl}genre/movie/list?api_key=8508a0bd1efc493c4bfa095b6a37f250&language=en-US`
      )
      .then((res) => setGenres(res.data.genres));
      setLoading(true);
  }, []);

  const getMoviesByGenre = (id) => {
    setCurrentGenre(id);
    axios
      .get(
        `${apiUrl}discover/movie?api_key=8508a0bd1efc493c4bfa095b6a37f250&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=${id}`
      )
      .then((res) => setMovies(res.data.results));
      setLoading(true);
  };

  // const handleAddToWatchlist = (movie) => {
  //   if (localStorage.getItem("watchlist")) {
  //     // get item from localstorage with watchlist key
  //     let watchlist = JSON.parse(localStorage.getItem("watchlist"));

  //     // add move into watchlist array
  //     watchlist = [...watchlist, movie];

  //     // Set new value to localstorage
  //     localStorage.setItem("watchlist", JSON.stringify(watchlist));
  //   } else {
  //     localStorage.setItem("watchlist", JSON.stringify([movie]));
  //   }
    // Add to localstorage, key = watchlist
  return (
    <Container className="mt-3 align-items-center">
      <Col md={12}>
      {genres.length !== 0 && loading ? (
        genres.map((genre) => (
          <Button
            size="sm"
            active
            key={genre.id}
            onClick={() => getMoviesByGenre(genre.id)}
            className="mr-3 mb-3"
            color={currentGenre === genre.id ? "warning" : "light"}
          >
            <strong>{genre.name}</strong>
          </Button>
        ))
      ) : (
        <Loading/>
      )}
      </Col>
      <Container className="d-flex align-items-center">
          <Row>
          {movies.length !== 0 ? (
            movies.map(movie => (
              <Col md={3} key={movie.id}>
                <Card style={{marginBottom: "15px", backgroundColor: "#E09F3E"}}>
                  <CardImg src={`${imgUrl}${movie.poster_path}`}></CardImg>
                  <CardBody className="d-flex flex-column align-items-center" style={{minHeight: "200px"}}>
                    <CardTitle style={{minHeight: "80px", marginTop:"15px"}}><strong>{movie.title}</strong></CardTitle>
                    <CardText>{movie.release_date}</CardText>
                    <Button 
                    block 
                    color="warning" 
                    className="mt-auto"
                    onClick={() => {history.push(`/detail/${movie.id}`);}}
                    style={{backgroundColor:"#335C67", color:"whitesmoke"}}
                    ><b>See Details</b></Button>
                  </CardBody>
                </Card>
              </Col>                     
          ))
        ) : (
          <Container></Container>
        )}
        </Row>
      </Container>
    </Container>
  );
}

export default AllMovies;