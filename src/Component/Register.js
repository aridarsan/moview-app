import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { 
  Button, 
  Modal, 
  ModalHeader, 
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input, } from 'reactstrap';

const Register = (props) => {
  const {
    className
  } = props;

  const [fullName, setFullName] = useState("");
  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();

  const url = "https://5fa4bcd2732de900162e85ef.mockapi.io/api/register ";
  const bodyData = {
    fullName: fullName,
    userName: userName,
    email: email,
    password: password,
  };

  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(bodyData)
  })
    .then((res) => res.json())
    .then((res) => console.log(res))
    .then(() =>
      history.push("/"));
    //   alert("your account has been created")
  };

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button className="mb-2 mr-2 mb-sm-0" color="danger" onClick={toggle}>Sign Up</Button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}><strong>Register now! to get started</strong></ModalHeader>
          <ModalBody>
            <Form onSubmit={handleSubmit}>
              <FormGroup>
                <Label for="fullName">Full Name</Label>
                  <Input type="text" name="fullName" id="fullName" placeholder="Input your full name." onChange={(e) => setFullName(e.target.value)}/> 
              </FormGroup>

              <FormGroup>
                <Label for="userName">User Name</Label>
                  <Input type="text" name="userName" id="userName" placeholder="Input your last name." onChange={(e) => setUserName(e.target.value)} /> 
              </FormGroup>

              <FormGroup>
                <Label for="email">Email</Label>
                  <Input type="email" name="email" id="email" placeholder="Input your email address." onChange={(e) => setEmail(e.target.value)}/>
              </FormGroup>

              <FormGroup>
                <Label for="password">Password</Label>
                  <Input type="password" name="password" id="password" placeholder="Input your password." onChange={(e) => setPassword(e.target.value)} /> 
              </FormGroup>

              <FormGroup>
                <Button block type="submit" style={{backgroundColor: "#E09F3E", color: "#222222"}}>Sign Up</Button>
              </FormGroup>
            </Form>
          </ModalBody>
      </Modal>
    </div>
  );
}

export default Register;